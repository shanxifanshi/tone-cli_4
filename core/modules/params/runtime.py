#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from utils.log import logger
from module_interfaces import PARAM

timeunit_map = {
    's': 1,
    'h': 3600,
    'm': 60,
}


class runtime(PARAM):
    def get(self, value):
        if value[-1].lower() in timeunit_map:
            self.value = int(float(value[0:-1]) * timeunit_map[value[-1]])
        else:
            self.value = int(value)
        return self.value
