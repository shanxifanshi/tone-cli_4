#!/usr/bin/env python
# -*- coding: utf-8 -*-


# from utils.log import logger
from module_interfaces import TESTMODULE
from utils.tests import TestCommands


class SystemInfoPost(TESTMODULE):
    def run(self, test_instance):
        testcmd = TestCommands(test_instance.config)
        testcmd.dump_system_info(ext='post')
