<div align="center">
<h1>tone command line</h1>
</div>

简体中文 | [English](README.md)

## 安装

```bash
make install
```

## 使用

以unixbench为例：
* 第一步：获取已集成的测试套

```bash
$ tone list
unixbench                                          performance
```
* 第二步：拉取测试套对应的仓库

```bash
$ tone fetch unixbench
```

* 第三步：安装测试套

```bash
$ tone install unixbench
```

* 第四步：执行测试套

```bash
$ tone run unixbench
```
