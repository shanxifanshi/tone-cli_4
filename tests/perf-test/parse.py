#!/usr/bin/env python
import sys
import re

reg = re.compile(r'[\d.]+:\s+(.*)\s*:\s*(FAILED!$|Ok$|Skip.*$)')
with sys.stdin as stdin:
    for index, line in enumerate(stdin.readlines()):
        result = reg.search(line)
        if result:
            (name, status) = result.groups()
            name = name.strip().replace(' ', '-')
            name = name.replace(':', '-')
            name = re.sub(r"[*'>&+,]", "", name)
            if status.startswith('FAILED'):
                status = 'Fail'
            elif status.startswith('Skip'):
                status = 'Skip'
            elif status.startswith('Ok'):
                status = 'Pass'
            else:
                print("Error: unrecognized status at {} {}".format(index, line))
                print(name, status)
                sys.exit(1)
            print("{}: {}".format(name, status))
