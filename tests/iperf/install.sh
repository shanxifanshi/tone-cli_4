GIT_URL="https://gitee.com/mirrors/iperf3.git"
DEP_PKG_LIST="gcc"

# fetch()

STRIP_LEVEL=1

build()
{
    logger scp -r "$TONE_BM_CACHE_DIR"/  "$TONE_BM_BUILD_DIR"
    logger ./configure --prefix="$TONE_BM_RUN_DIR"
    logger make
}

install()
{
    logger make install
    logger ldconfig
}
    
