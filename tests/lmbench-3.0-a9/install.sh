DEP_PKG_LIST="patch make gcc rpcbind parted"
#WEB_URL="https://nchc.dl.sourceforge.net/project/lmbench/development/lmbench-3.0-a9/lmbench-3.0-a9.tgz"

build() {
    #cd $(basename ${WEB_URL%.*})
    cp $TONE_BM_SUITE_DIR/lmbench-3.0-a9.tgz  $TONE_BM_BUILD_DIR/
    cd $TONE_BM_BUILD_DIR/
    tar -xvf lmbench-3.0-a9.tgz
    cd lmbench-3.0-a9
    rpcs=$(rpm -ql glibc-headers | grep 'rpc/rpc.h' | wc -l)
    if [[ $rpcs -lt 1 ]];then
        DEP_PKG_LIST+=" libtirpc-devel" install_pkg
        patch -p1 <$TONE_BM_SUITE_DIR/rpc_patch/fix_rpc_error.patch
    fi

    if [[ $(arch)=="aarch64" ]];then
       sed -i 's/arm\*/aarch64\*/' scripts/gnu-os 
    fi

    make
}

install() {
	cp -rf * $TONE_BM_RUN_DIR/
}
