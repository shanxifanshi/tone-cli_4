#!/bin/bash

run()
{
    cd "$TONE_BM_RUN_DIR" ||exit
    logger ./stream_c.exe
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}
