GIT_URL="https://github.com/Mellanox/sockperf.git"
BRANCH="sockperf_v2"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST="autoconf automake gcc g++ libtool"
else
	DEP_PKG_LIST="autoconf automake gcc gcc-c++ libtool"
fi

build()
{
  ./autogen.sh
  ./configure --prefix="$TONE_BM_RUN_DIR"
  make
}

install()
{
  make install
}