GIT_URL="https://github.com/akopytov/sysbench.git"
BRANCH="1.0"


DEP_PKG_LIST="vim make automake libtool pkgconfig libaio-devel"

build() {
	./autogen.sh
	./configure --prefix=$TONE_BM_RUN_DIR --without-mysql
	make
}

install() {
	make install
}
