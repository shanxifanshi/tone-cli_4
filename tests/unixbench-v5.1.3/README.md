# UnixBench

## 中文

**UnixBench**旨在提供一个基本的类Unix系统的性能指标。

### 相关链接
https://github.com/kdlucas/byte-unixbench

## 使用说明

使用过程中遇到无法获取github上的仓库时，可以修改`install.sh`中`GIT_URL`为国内仓库地址或私有仓库地址，如：

https://gitee.com/mirrors_aliyun/byte-unixbench.git

### 参数说明

unixbench.conf参数：
* test: 测试名称
* nr_task: 线程数量
* iterations: 运行次数

### 结果

* score: 性能测试得分

## English

The purpose of UnixBench is to provide a basic indicator of the performance of a Unix-like system;

### Homepage

https://github.com/kdlucas/byte-unixbench

### Notes

If your testbed can not get the repository from the Github, you can modify the `GIT_URL` in `install.sh` to a domestic repository or private repository. such as:

https://gitee.com/mirrors_aliyun/byte-unixbench.git

### Parameters:

unixbench.conf:
* test: test name
* nr_task: thread numbers
* iterations: run times

### Result

* score: performance score