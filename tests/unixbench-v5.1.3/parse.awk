#!/usr/bin/awk -f

/^Dhrystone 2 using register variables/ && $NF ~ /[0-9.]+/ {
    score_dhry2reg[dhry2reg_matched++] = $NF
}
/^Double-Precision Whetstone/ && $NF ~ /[0-9.]+/ {
    score_whetstone_double[whetstone_double_matched++] = $NF
}
/^Execl Throughput/ && $NF ~ /[0-9.]+/ {
    score_execl[execl_matched++] = $NF
}
/^File Copy 1024 bufsize 2000 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fstime[fstime_matched++] = $NF
}
/^File Copy 256 bufsize 500 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fsbuffer[fsbuffer_matched++] = $NF
}
/^File Copy 4096 bufsize 8000 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fsdisk[fsdisk_matched++] = $NF
}
/^Pipe Throughput/ && $NF ~ /[0-9.]+/ {
    score_pipe[pipe_matched++] = $NF
}
/^Pipe-based Context Switching/ && $NF ~ /[0-9.]+/ {
    score_context1[context1_matched++] = $NF
}
/^Process Creation/ && $NF ~ /[0-9.]+/ {
    score_spawn[spawn_matched++] = $NF
}
/^Shell Scripts \(1 concurrent\)/ && $NF ~ /[0-9.]+/ {
    score_shell1[shell1_matched++] = $NF
}
/^Shell Scripts \(8 concurrent\)/ && $NF ~ /[0-9.]+/ {
    score_shell8[shell8_matched++] = $NF
}
/^System Call Overhead/ && $NF ~ /[0-9.]+/ {
    score_syscall[syscall_matched++] = $NF
}
/^System Benchmarks Index Score[[:space:]]+[0-9.]+$/ {
    score_total[total_matched++] = $NF
}

END {
    if (dhry2reg_matched == 1)
        printf("Dhrystone_2_using_register_variables: %.2f\n", score_dhry2reg[0])
    if (whetstone_double_matched == 1)
        printf("Double-Precision_Whetstone: %.2f\n", score_whetstone_double[0])
    if (execl_matched == 1)
        printf("Execl_Throughput: %.2f\n", score_execl[0])
    if (fstime_matched == 1)
        printf("File_Copy_1024_bufsize_2000_maxblocks: %.2f\n", score_fstime[0])
    if (fsbuffer_matched == 1)
        printf("File_Copy_256_bufsize_500_maxblocks: %.2f\n", score_fsbuffer[0])
    if (fsdisk_matched == 1)
        printf("File_Copy_4096_bufsize_8000_maxblocks: %.2f\n", score_fsdisk[0])
    if (pipe_matched == 1)
        printf("Pipe_Throughput: %.2f\n", score_pipe[0])
    if (context1_matched == 1)
        printf("Pipe-based_Context_Switching: %.2f\n", score_context1[0])
    if (spawn_matched == 1)
        printf("Process_Creation: %.2f\n", score_spawn[0])
    if (shell1_matched == 1)
        printf("Shell_Scripts_(1_concurrent): %.2f\n", score_shell1[0])
    if (shell8_matched == 1)
        printf("Shell_Scripts_(8_concurrent): %.2f\n", score_shell8[0])
    if (syscall_matched == 1)
        printf("System_Call_Overhead: %.2f\n", score_syscall[0])
    if (total_matched == 1)
        printf("System_Benchmarks_Index_Score: %.2f\n\n", score_total[0])
}
