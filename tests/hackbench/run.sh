setup()
{
	# default 40 fds/group
	fds=40
	tasks=$((nr_task * fds))

	# increase permitted open file number
	ulimit -n $((tasks + 2048))

	def_pid_max=32768
	[ "$tasks" -gt $((def_pid_max / 2)) ] && {
		sysctl -w kernel.pid_max=$((tasks * 2))
		sysctl -w kernel.threads-max=$((tasks * 2))
		sysctl -w vm.max_map_count=$((tasks * 100))
	}
}

run()
{
	[ "$ipc" = 'pipe' ] && ipc_option='--pipe'
	export PATH=$TONE_BM_RUN_DIR/bin:$PATH

	[ -n "$datasize" ] || datasize=100
	logger hackbench --datasize ${datasize} -g "$nr_task" --$mode "$ipc_option" -l $loop
}

parse()
{
	grep "Time:" | awk '{printf "Time: %.3f\n",$2}'
}