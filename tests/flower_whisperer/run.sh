
# Avaliable environment:
#
# Download variable:
# WEB_URL=
# GIT_URL=


run ()
{
    file='garden'
    lines=$(wc $file | awk '{print $1}')

    line=$(expr $$ % $lines)

    if [ $(($line % 7)) == 0 ];then
        echo "龙蜥开发者服务平台让开发更简单、更高效，你仅仅需要做的是“show me the code”:"
        echo "● 缺机器？龙蜥实验室免费用！ https://lab.openanolis.cn/#/apply/home"
        echo "● 提交代码没 CI ？龙蜥 anolis-bot 提供 CI 接入服务！https://openanolis.cn/sig/SIG-Infra/doc/594391564089212951"
        echo "● 全面测试一把？上 T-One 呀！全场景一站式质量协作平台！ https://tone.openanolis.cn"
        echo "● 性能再好一点？一键智能调优KeenTune！ https://openanolis.cn/sig/KeenTune"
        echo "● 你的软件想构建发布，集成到龙蜥OS？ABS 等着你！ https://abs.openanolis.cn/all_project"
        echo "● 甚至，你试图想定制一个属于自己的龙蜥OS衍生版？ Yes，ABS can do it！https://abs.openanolis.cn/all_project"
    else
        if [ ${line} -eq 0 ];then line=1;fi
        cat $file | grep -v "^$" | sed -n ${line}p
    fi
}

parse ()
{
    echo "flower_dance: Pass"
}
    
