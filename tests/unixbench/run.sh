#!/bin/bash
. $TONE_ROOT/lib/disk.sh
. $TONE_ROOT/lib/testinfo.sh

init_disk()
{
	[ -n "$SKIP_DISK_SETUP" ] && return
	nr_disk=1
  fs=${fs:-ext4}
  disk_type=${disk_type:-disk}
  setup_disk_fs "$disk_type" "$nr_disk" "mounted"
}

run()
{
	add_testinfo_cpu_model
	if ! [ "${test}X" == "defaultX" ]; then
		init_disk
		fsdir=$(get_mount_points)

		# runtime / 10 to get the iteration
		# nr_task the parallel threads numbers
		# test the test name for running
		export LANG=C
		[ -n "$nr_task" ] && params="-c $nr_task" || params=""
		[ -n "$iterations" ] && params="$params -i $iterations"

    [[ $test =~ fstime|fsbuffer|fsdisk ]] && export UB_TMPDIR=$fsdir
    logger ./Run $test $params  || exit 1
  else
  	logger ./Run -c $nr_task
  fi
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
