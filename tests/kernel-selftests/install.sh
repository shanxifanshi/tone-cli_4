GIT_URL="https://gitee.com/anolis/cloud-kernel.git"
DEP_PKG_LIST="gcc make bc patch rsync iproute-tc llvm-toolset glibc-static popt-devel libcap-ng-devel libcap-devel fuse-devel fuse numactl-devel elfutils-libelf-devel"

get_kernel_version() {
    version=$(uname -r | awk -F "-" '{print $1}')
    echo ${version%.*}
}

kernel_version=$(get_kernel_version)
kernel_branch="devel-$kernel_version"

fetch() {
    timeout 60m git clone -q --depth 1 -b $kernel_branch $GIT_URL $TONE_BM_CACHE_DIR
}

build() {

    cd $TONE_BM_CACHE_DIR
    patchpath="$TONE_BM_SUITE_DIR/patch/$kernel_version"

    # subcase return code  not 0,2 --> exitcode=1
    [ -f $patchpath/pmtu.sh.patch ] && patch -p1 < $patchpath/pmtu.sh.patch
    # fix kernel change
    [ -f $patchpath/mincore.patch ] && patch -p1 < $patchpath/mincore.patch

    make -C tools/testing/selftests
}

prepare_skip_list() {

    # skip breakpoints case if arch is x86_64
    [ "$(uname -m)" == "x86_64" ] && skip_test_list="breakpoints"
    # skip gpio test if CONFIG_GPIO_MOCKUP is not set
    grep CONFIG_GPIO_MOCKUP /boot/config-$(uname -r) | grep -q "is not set" && skip_test_list="$skip_test_list gpio"
    # skip dma_heap test if CONFIG_DMABUF_HEAPS is not set
    grep CONFIG_DMABUF_HEAPS /boot/config-$(uname -r) | grep -q "is not set" && skip_test_list="$skip_test_list dmabuf-heaps"
    echo "the final skip_test_list is : $skip_test_list"

    # skip install test
    for test in $skip_test_list; do
        sed -i "/$test/ s/^/#/" Makefile
    done
}

install() {
    cd $TONE_BM_CACHE_DIR/tools/testing/selftests/
    prepare_skip_list
    ./kselftest_install.sh $TONE_BM_RUN_DIR
}
