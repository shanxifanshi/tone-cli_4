DEP_PKG_LIST="java-1.8.0-openjdk-headless java-1.8.0-openjdk-devel java-1.8.0-openjdk"
#WEB_URL=""
#STRIP_LEVEL=1

build()
{
    cp $TONE_BM_SUITE_DIR/SPECjvm2008_1_01_setup.jar  $TONE_BM_BUILD_DIR/
    cp $TONE_BM_SUITE_DIR/SETUP_CONFIG  $TONE_BM_BUILD_DIR/
}

install()
{
    cd $TONE_BM_BUILD_DIR/
    java -jar SPECjvm2008_1_01_setup.jar -i console  < SETUP_CONFIG
}

