# SPECjvm2008
## Description
SPECjvm2008是一个观测JRE运行性能的基准测试套件。它的测试用例涵盖了大部分java基础应用场景，是架构选型和VM性能评测不可多得的利器。

## Homepage
[https://www.spec.org/jvm2008/](https://www.spec.org/jvm2008/)

## Version
SPECjvm2008

## Category
performance

## Parameters
- mode：模式(base:不允许做任何jvm参数调整的基准测试;peak:允许添加jvm调优参数的峰值测试)

## Results
```
startup.helloworld: 107.910000 ops/m
startup.compiler.compiler: 8.620000 ops/m
startup.compress: 21.460000 ops/m
startup.crypto.aes: 8.890000 ops/m
startup.crypto.rsa: 39.270000 ops/m
startup.crypto.signverify: 29.810000 ops/m
startup.mpegaudio: 12.000000 ops/m
startup.scimark.fft: 42.700000 ops/m
startup.scimark.lu: 49.140000 ops/m
startup.scimark.monte_carlo: 20.120000 ops/m
startup.scimark.sor: 23.100000 ops/m
startup.scimark.sparse: 21.460000 ops/m
startup.serial: 11.440000 ops/m
startup.sunflow: 13.950000 ops/m
startup.xml.transform: 1.830000 ops/m
startup.xml.validation: 17.510000 ops/m
compiler.compiler: 336.710000 ops/m
compress: 142.880000 ops/m
crypto.aes: 46.720000 ops/m
crypto.rsa: 853.060000 ops/m
crypto.signverify: 320.520000 ops/m
derby: 319.750000 ops/m
mpegaudio: 91.740000 ops/m
scimark.fft.large: 60.470000 ops/m
scimark.lu.large: 30.310000 ops/m
scimark.sor.large: 29.400000 ops/m
scimark.sparse.large: 37.230000 ops/m
scimark.fft.small: 323.270000 ops/m
scimark.lu.small: 569.030000 ops/m
scimark.sor.small: 136.320000 ops/m
scimark.sparse.small: 134.520000 ops/m
scimark.monte_carlo: 163.840000 ops/m
serial: 97.380000 ops/m
xml.transform: 221.010000 ops/m
xml.validation: 488.440000 ops/m
```

## Manual Run
请参考官方文档

