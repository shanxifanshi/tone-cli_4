#!/bin/bash

ltp_blacklist=$TONE_BM_SUITE_DIR/ltp.blacklist

get_kernel_info()
{
	kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
	os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')
	arch=$(uname -m)
}

get_kernel_info

prepare_for_blacklist()
{
	# tpci testcase run on ecs will let the machine hanged, skip it
        systemd-detect-virt --vm -q && grep -q "^tpci$" $ltp_blacklist || echo "tpci" >>$ltp_blacklist
        
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=2213
        if [ x"$os_ver" == x"an7" ]; then
		an7_skip_list=$(grep -v '^#' $TONE_BM_SUITE_DIR/ltp.an7.blacklist)
		for an7_case in $an7_skip_list
		do
			grep -q "^$an7_case$" $ltp_blacklist || echo "$an7_case" >>$ltp_blacklist
		done
        fi

	if [ x"$kernel_ver" == x"4.19" -a x"$arch" == x"aarch64" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=2202
		grep -q "^pty04$" $ltp_blacklist || echo "pty04" >>$ltp_blacklist
		grep -q "^cve-2020-11494$" $ltp_blacklist || echo "cve-2020-11494" >>$ltp_blacklist
	fi

	if [ x"$os_ver" == x"an8" -a x"$kernel_ver" == x"5.10" -a  x"$arch" == x"x86_64" ]; then
		# https://bugzilla.openanolis.cn/show_bug.cgi?id=2213
		grep -q "^macsec02$" $ltp_blacklist || echo "macsec02" >>$ltp_blacklist
		grep -q "^macsec03$" $ltp_blacklist || echo "macsec03" >>$ltp_blacklist
	fi
}

setup_for_net()
{	
	if [ "$group" == "net.sctp" ]; then
		lsmod | grep -q sctp || modprobe sctp && sctp_flag=0				
	fi
}

cleanup_for_net()
{	
	if [ "$group" == "net.sctp" -a $sctp_flag ]; then
		modprobe -r sctp
	fi
}

setup()
{
	echo 1    > /proc/sys/kernel/panic
	echo 1    > /proc/sys/kernel/softlockup_panic
	echo 50   > /proc/sys/kernel/watchdog_thresh
	echo 1    > /proc/sys/kernel/hung_task_panic
	echo 1200 > /proc/sys/kernel/hung_task_timeout_secs
	systemctl start kdump.service
	prepare_for_blacklist
	setup_for_net
}

run()
{
	if [ "$group" = all ]; then
		logger ./runltp -S $ltp_blacklist
	else
		logger ./runltp -f $group -S $ltp_blacklist
	fi
}

parse()
{
	awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
	cleanup_for_net
	exit 0
}
