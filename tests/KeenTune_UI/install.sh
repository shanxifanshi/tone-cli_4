#!/bin/bash

set_python_env()
{
    pip3 install selenium
    if [ $? -ne 0 ];then
        echo "selsenium Installation failed"
    fi
}

install_chrome_env()
{
    rpm -ql google-chrome-stable && return 0
    yum install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
    if [ $? = 0 ];then
        wget http://npm.taobao.org/mirrors/chromedriver/2.41/chromedriver_linux64.zip
    fi

    if [ $? = 0 ];then
        unzip chromedriver_linux64.zip
    fi

    mv chromedriver /usr/bin/
    chmod +x /usr/bin/chromedriver
    rm -rf chromedriver_linux64.zip
}

get_keentune_code()
{
    cd $TONE_BM_RUN_DIR/
    keentune_addr="https://gitee.com/anolis/keentuned.git"
    git clone -b $keentune_branch $keentune_addr
}

build()
{
    set_python_env
    install_chrome_env
    get_keentune_code
}

install()
{
    echo "The test environment is ready"
}