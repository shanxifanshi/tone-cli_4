#!/bin/bash

. $TONE_ROOT/lib/testinfo.sh
. $TONE_ROOT/lib/common.sh

clear_keentuneUI_env()
{
    rm -rf /usr/bin/chromedriver && yum remove google-chrome-stable.x86_64 -y
    if [ $? = 0 ]; then
      yum list google-chrome-stable.x86_64 && echo "Uninstall the success"
    fi
}

setup()
{
    echo "this is run KeenTune UI test cases!"
}

run()
{
    cd $TONE_BM_RUN_DIR/
    logger cd $TONE_BM_RUN_DIR/keentuned/test/KeenTune_UI
    echo -e "Start test:python3 main.py\nTests will ta severkeal minutes, pls wait"
    python3 main.py $web_ip

    if [ $? -ne 0 ]; then
        cd $TONE_BM_RUN_DIR/
        echo "ERROR:python3 main.py failed"
    fi

    cd $TONE_BM_RUN_DIR/
}

parse()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
    clear_keentuneUI_env
}