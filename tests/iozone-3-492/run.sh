#!/bin/sh
FPATH=${FPATH:-/home/iozone.data}

run()
{
    sz=$(bytes_convert ${memsize} g)
    recordsize=${recordsize:-16m}
    echo "$(date) iozone start ${sz}"
    logger ./iozone -i 0 -i 1 -i 2 -s ${sz} -r ${recordsize} -f ${FPATH}
    echo "$(date) iozone end ${sz}"

    logger sync
    logger "echo 3 > /proc/sys/vm/drop_caches"
    sleep 30
}

teardown()
{
    rm -f ${FPATH}
}


parse()
{
    $TONE_BM_SUITE_DIR/parse.awk
}