#WEB_URL="https://www.iozone.org/src/current/iozone3_492.tgz"

build()
{
    cp $TONE_BM_SUITE_DIR/iozone3_492.tgz  $TONE_BM_BUILD_DIR/
    cd $TONE_BM_BUILD_DIR/
    tar -xvf iozone3_492.tgz
    cd ./iozone3_492/src/current
    if [[ $(/bin/arch) == 'x86_64' ]];then
        make linux-AMD64
    else
        make linux
    fi
}

install()
{
    cp -rf * $TONE_BM_RUN_DIR/
}
